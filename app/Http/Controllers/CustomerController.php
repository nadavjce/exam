<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Collection;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if (Gate::denies('salesrep')) {
            $id = Auth::id();
            $user = User::Find($id);
            $customers = Customer::Get();
            return view('customers.index', ['customers' => $customers]);
       }

           $id = Auth::id();
            $user = User::Find($id);
            $customers = $user->customers;;
         //   return view('cars.index', ['cars' => $cars]);  
            return view('customers.index', compact('customers'));
         
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*if (Gate::denies('salesrep')) {
            abort(403,"Sorry you are not allowed to create");
        }*/
    return view('customers.create');
             
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
       /* if (Gate::denies('salesrep')) {
            abort(403,"Sorry you are not allowed");
        }*/
       /* $this->validate($request, [
            'brand' => 'required',
            'year' => 'required',
            'price' => 'required',
        ]);*/
             $customers = new Customer();   
            $customers->name = $request->name; 
            $customers->email = $request->email;
            $customers->phone = $request->phone;
            $customers->user_id = Auth::id();
            $customers->created_at = null;
            $customers->updated_at = null;
            $customers->save();
            return redirect('customers');
                }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to edit ");
        }  
        // if()
            $customers = Customer::find($id);
             return view('customers.edit', compact('customers','id'));     
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*if (Gate::denies('salesrep')) {
            abort(403,"Sorry you are not allowed to update");
        }*/
            $customers = Customer::findOrFail($id);
       // לא עובד if(!$book->user->id == Auth::id()) return(redirect('books'));
        //test if title is dirty
      //  $customers->update($request->except(['_token']));
      
        $customers-> update($request->all());
        //$customers->status = update($request->status);
        
            return redirect('customers');   
            
            
            // if($request->ajax()){
               
            //        return Response::json(array('result' => 'success1','status' => $request->status ), 200);
            //    } else {          
            //        return redirect('customers');
            //    }        
        }
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed");
        }
     
                
                $customers = Customer::find($id);
                $customers->delete();
                return redirect('customers');
                
        
         
        }

        public function done($id)
    {
        //only if this todo belongs to user 
        if (Gate::denies('manager')) {
            abort(403,"You are not allowed to mark customers deal closed..");
         }          
        $customers = Customer::findOrFail($id);            
        $customers->status = 1; 
        $customers->save();
        return redirect('customers');    
    }    

    public function name($id)
    {
        //only if this todo belongs to user 
                 
        $name = User::find($id);            
        $name->name->$id; 
       
        return redirect('customers', compact('customers','name'));    
    }    
    
       
        // sorted method
  /*      public function LowtoHigh()
        {
            $sorted = Customer::All();
            $cars = $sorted->sortBy('price');
        // if i want to sort from high to low
          //  $cars = $sorted->sortByDesc()('price');
            return view('cars.index', compact('cars'));
        }

        public function HigetoLow()
        {
            $sorted = Customer::All();
            $cars = $sorted->sortByDesc('price');
            return view('cars.index', compact('cars'));
        }
    
        public function avg()
        {
            $cars = Customer::All();
            $avg =  $cars->avg('price');
            return view('cars.index', compact('cars','avg'));
        }

     /*   public function search(Request $request)
        {
            $allcars = Customer::All();
            $cars = $allcars->whereBetween('price', [$request->budget_from, $request->budget_to]);
            return view('cars.index', compact('cars'));
        }*/
   
}
