<head>

<style>
    h1 {
      color: white;
      text-align: center;
      text-decoration: underline;
    }
    .form-1{
      width:10%;
      margin-left:50%;
      margin-right:50%
    }
    h3 {
      color: white;
      text-align: center;
    }
          table {
            margin-top: 50px;
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }

          td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            
          }

          tr:nth-child(even) {
            background-color: #dddddd;
            font-weight:bold;
      
          }
          tr:nth-child(odd){
            background-color: #aaaaaa;
            font-weight:bold;

          }
          tr:nth-child(even):hover {
            background-color: gray;
      
          }
          tr:nth-child(odd):hover {
            background-color: gray;
      
          }

</style>
</head>

@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<body>
<h1>This is the customer list</h1>




        
        <a href = "{{route('customers.create') }}"> <h1> create new customer </h1> </a> 
         <table>
                      <tr>
                        <th>Name</th>
                        <th>email</th>
                        <th>phone</th>
                        <th>edit</th>
                        @can('manager')   <th>Delete customer</th>@endcan
                        @cannot('manager') <th>Delete customer</th> @endcannot  
                        @can('manager')   <th>deal closed</th>@endcan
                       
                      </tr>
                              
                    
         

            @foreach($customers as $customer)
            <tr>
            <td>{{$customer->name}}</td>
        
             <td>{{$customer->email}}</td>
             <td>{{$customer->phone}}</td>
             <td><a href="{{route('customers.edit',$customer->id)}}"> edit</a></td>   
             @can('manager') <td><a href="{{route('customers.edit',$customer->id)}}"> delete</a></td> >@endcan
           
             @cannot('manager') <td> delete</a></td>   @endcannot  
             @can('manager')
             <td>
            
             @if ($customer->status == 0)
             <a href="{{route('done', $customer->id)}}">deal closed</a>
             @else
             
             $('td', nrow).css('background-color', 'Green');
             @endif
            
   
       </td> @endcan
       
     
            @endforeach
     </table>

     
      


<br><br>
  

</body>


<script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('customers')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   //data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}), change between 1 and 0
                   data: JSON.stringify({'status':(event.target.value), _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
   </script>

@endsection