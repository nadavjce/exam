@extends('layouts.app')
@section('content')


<h1> Edit car </h1>
<form method = 'post' action = "{{action('CustomerController@update', $customers->id)}}"  >
@csrf
@method('PATCH')
<div class = "form-group">

    <label for = "title"> name: </label>
    <input type = "text" class = "form-control" name = "name" value = "{{$customers->name}}">
    <label for = "title"> email:</label>
    <input type = "email" class = "form-control" name = "email" value = "{{$customers->email}}">
    <label for = "title"> phone number </label>
    <input type = "number" class = "form-control" name = "phone" value = "{{$customers->phone}}">
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Update">
</div>
</form>
@can('manager')
<form method = 'post' action = "{{action('CustomerController@destroy', $customers->id)}}"  >
@csrf
@method('DELETE')
<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "Delete">
</div>
</form>
@endcan
@endsection