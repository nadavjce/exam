@extends('layouts.app')
@section('content')

<h1> Add a new customer </h1>
<form method = 'post' action = "{{action('CustomerController@store')}}"  >
{{csrf_field()}}

<div class = "form-group">
    <label for = "name"> customer name: </label>
    <input type = "text" class = "form-control" name = "name">
    <label for = "year"> customer email </label>
    <input type = "email" class = "form-control" name = "email">
    <label for = "phone"> customer phone number </label>
    <input type = "number" class = "form-control" name = "phone">
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "save">
</div>

<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

</form>

@endsection