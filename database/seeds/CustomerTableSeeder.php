<?php

use Illuminate\Database\Seeder;

class CustomerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert(
            [
                [
                    'name' => 'customer1',
                    'email' => 'a@a.com',
                    'phone'=>'1',
                   
                    'created_at' => date('Y,m,d G:i:s'),
            
                ],
               
            ]);
    }
}
