<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                    'name' => 'manager',
                    'email' => 'a@a.com',
                    'id'=>'1',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role'=>'manager',
                ],
                [
                    'name' => 'salesrep1',
                    'email' => 'b@b.com',
                    'id'=>'2',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role'=>'salesrep',
                ],
                [
                    'name' => 'salesrep2',
                    'email' => 'c@c.com',
                    'id'=>'3',
                    'password'=> Hash::make('12345678'),
                    'created_at' => date('Y,m,d G:i:s'),
                    'role'=>'salesrep',
                ],
            ]);
    }
}
